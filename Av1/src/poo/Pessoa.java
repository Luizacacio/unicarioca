package poo;

public class Pessoa implements Comparable<Pessoa> {
	
	public int id, idade;
	public String nome;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Pessoa(int id, int idade, String nome ) {
		dados(id, idade, nome);
	}
	
	public Pessoa() {}
		
	public void dados(int identidade, int age, String name) {
			nome = name;
			id = identidade;
			idade = age;
		
	}
	
	@Override
	public int compareTo(Pessoa p) {
		
		if(this.idade < p.getIdade()) {
			return -1;
		}
		if(this.idade > p.getIdade()) {
			return 1;
		}
		return 0;
	}
	
	public void print() {
		System.out.println("\r------------------------------------");
		System.out.print(" ID: "+this.id+" |");
		System.out.print(" Nome: "+this.nome +" |");
		System.out.print(" Idade: "+this.idade);
		System.out.println("\r------------------------------------");
	}
	
	
	

}
