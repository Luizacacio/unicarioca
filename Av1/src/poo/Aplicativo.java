package poo;
import java.util.*;

public class Aplicativo {
	
	static ArrayList<Pessoa> lista = new ArrayList<Pessoa>();
	
    static Scanner tecla = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		int op;
		
		lista.add(new Pessoa(1, 10, "Jo�o"));
		lista.add(new Pessoa(2, 5, "Alice"));
		lista.add(new Pessoa(3, 27, "Fernando"));
		lista.add(new Pessoa(4, 12, "Carlos"));
		lista.add(new Pessoa(5, 31, "Priscila"));		
		
		do {
			System.out.println("\r-------- MENU --------");
			System.out.println("1. Ordenar pela idade");
			System.out.println("2. Consultar por ID");
			System.out.println("3. Consultar Cache");
			System.out.println("4. Sair");
			System.out.print("Op��o: ");
			op = tecla.nextInt();
			switch(op) {
			case 1: ListarOrdem(); break;
			case 2: ConsultaPessoa(); break;
			case 3: BuscarCache(); break;
			case 4: break;
			}
		}while(op != 4);
			
	}
	
	public static void Ordenar() {
		Collections.sort(lista);
	}
	
	public static void imprimir(Pessoa p) {
		p.print();
	}
	
	public static void ListarOrdem() {
		System.out.println("Lista ordenada pela idade");
		System.out.println("------------------------------------");
		
		Ordenar();
		
		for(Pessoa p : lista) {
			
			if(!p.equals(null)) {
				imprimir(p); 
			}
		}
	}
	
	public static void ConsultaPessoa() {
		System.out.println("Insira o ID da pessoa a consultar:");
		int x = tecla.nextInt();
		
		for(Pessoa p : lista) {
			if(x == p.getId()) {
				imprimir(p);
			}
			
		}
	}
	
	public static void BuscarCache() {
		System.out.print("Insira o ID para a consulta no cache: ");
		int id = tecla.nextInt();
		CachePessoa.RetornaPessoa(id);
	}
			
}
