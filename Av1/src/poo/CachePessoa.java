package poo;
import java.util.*;

public class CachePessoa extends Aplicativo{
	
	static ArrayList<Integer> index = new ArrayList<>();
	static Scanner tecla = new Scanner(System.in);
	
	public static void RetornaPessoa(int id) {
		
		if(index.contains(id)) {
			BuscaCache(id-1);
		}else {
			index.add(id);
			for(Pessoa p : lista) {
				if(id == p.getId()) {
					Aplicativo.imprimir(p);
				}
			}
			
			
		}
	}
	
	public static void BuscaCache(int x) {
		System.out.println("------------------------------------");
		System.out.println("ID ENCONTRADO NA MEMORIA");
		System.out.println("------------------------------------");
		Aplicativo.imprimir(lista.get(x));
	}

}
