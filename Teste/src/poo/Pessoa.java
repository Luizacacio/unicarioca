package poo;

public class Pessoa {
	
	private int CPF;
	private String nome;
	
	public int getCPF() {
		return CPF;
	}

	public void setCPF(int cPF) {
		CPF = cPF;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	
	
	public Pessoa(String nome, int id) {
		this.nome = nome;
		this.CPF = id;
		
	}
	
	//construtor
	public Pessoa(int CPF, String nome) {
		this.CPF = CPF;
		this.nome = nome;
	}

}
