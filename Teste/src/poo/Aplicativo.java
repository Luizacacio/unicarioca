package poo;
import java.util.Scanner;

//Array de pessoas

public class Aplicativo {
	
	//Constante
	static final int maxppl = 5;
	
	//vari�vel comum
	static int n = 0;
	
	//Lista de pessoas
	static Pessoa[] lista = new Pessoa[maxppl];
	
	static Scanner tecla = new Scanner(System.in);
	

	public static void main(String[] args) {
		
		int op;
		do {
			System.out.println("-----MENU-----");
			System.out.println("1. Incluir Pessoa");
			System.out.println("2. Listar Pessoas");
			System.out.println("3. Sair");
			System.out.println("Digite sua escolha: ");
			op = tecla.nextInt();
			switch(op) {
			case 1: IncluiPessoa(); break;
			case 2: ListaPessoa(); break;
			case 3: break;
			}
		}while (op!=3);

	
	
	
	}
	
	public static void IncluiPessoa() {
		
		System.out.println("Digite o nome: ");
		String nome = tecla.nextLine();
		System.out.println("Digite o CPF: ");
		int CPF = tecla.nextInt();
		
		lista[n++] = new Pessoa(nome , CPF);
		System.out.println("Pessoa cadastrada.");
	}
	
	public static void ListaPessoa() {
		for (int i = 0; i <lista.length-1; i++) {
			if(lista[i] != null){
				System.out.println("Nome: "+lista[i].getNome() +"CPF: "+lista[i].getCPF());
			}else {
				break;
			}
			
		}
		
	}
}
